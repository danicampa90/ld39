﻿using System;

namespace Assets.Script.Scorekeeping
{

    [Serializable]
    public class Score
    {
        //curl --data '{"name":"bcsea", "score":2, "kills":2, "metal": 30}' -H "Content-Type: application/json" http://192.168.1.128:5000/scores

        public string name;
        public int score;
        public int kills;
        public int metal;
    }
}