﻿
using System;
using System.Collections.Generic;

namespace Assets.Script.Scorekeeping
{
    internal class TopScores
    {
        public static Score[] Scores;
        public static int myPosition;
        public static Score myScore;

        internal static void Reset()
        {
            Scores = null;
            myScore = null;
            myPosition = 0;
        }

        //NOTE!!! THIS IS NOT RESET!
        public static string Name = null;
    }
}