﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Script.Scorekeeping
{
    public class ScoreAPI: MonoBehaviour
    {
        public void SendScores(Score myscores, Action onComplete, Action onTimeout)
        {
            StartCoroutine(PostCoroutine(myscores, onComplete, onTimeout));
        }

        private IEnumerator PostCoroutine(Score myscore, Action onComplete, Action onTimeout)
        {

            Debug.LogFormat("Sending score for {0}: {1}...", myscore.name, myscore.score);
            TopScores.myScore = myscore;

            string json = JsonUtility.ToJson(myscore);
            var req = UnityWebRequest.Put("http://danicampa90.hopto.org:1337/scores", json);
            req.method = "POST";
            req.timeout = 3;
            yield return req.Send();
            if (req.isNetworkError || req.isHttpError)
                onTimeout();
            else
            {
                try
                {
                    TopScores.myPosition = JsonUtility.FromJson<MyPosition>(req.downloadHandler.text).positon + 1;

                    Debug.LogFormat("Sending score ... SUCCESS! Position{0}",TopScores.myPosition);
                }
                catch (Exception ex)
                {

                    Debug.LogErrorFormat("Error: {0}", ex.Message);

                    TopScores.myPosition = -1;
                }
            }


            Debug.LogFormat("Retrieving scores...", myscore.name, myscore.score);

            var req2 = UnityWebRequest.Get("http://danicampa90.hopto.org:1337/scores");
            req2.timeout = 3;
            yield return req2.Send();

            if (req2.isNetworkError || req2.isHttpError)
                onTimeout();
            else
            {
                try
                {
                    TopScores.Scores = JsonHelper.FromJson<Score>(req2.downloadHandler.text);

                    Debug.LogFormat("Retrieving scores... SUCCESS: {0} retrieved!", TopScores.Scores.Length);
                }
                catch (Exception ex)
                {

                    Debug.LogErrorFormat("Error: {0}", ex.Message);
                    onTimeout();
                    yield break;
                }
                onComplete();
            }

        }

        [Serializable]
        private class MyPosition
        {
            public int positon;
        }
    }
}
