﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Assets.Script.Model;

public class EnemySpawner : MonoBehaviour {

    
	// Use this for initialization
	void Start () {
        Map.Instance.OnTick += Instance_OnTick;
	}

    float randomsEnemyCounter = 0; // makes the remainder of an enemy available to next tick instead of switching directly from 0 enemies /tick to 1 enemy per tick
    int wavetimer = ConstantsGameBalance.EnemyWaveEveryTicks;
	
    private void Instance_OnTick()
    {

        // TODOLOW: inefficient
        var numOfPlayerStructs = Map.Instance.structures.Count((str) => str is IPlayerStructure);
        if (numOfPlayerStructs < ConstantsGameBalance.EnemyStartAtStructureNumber)
            return;

        SpawnRandomEnemies(numOfPlayerStructs);

        wavetimer--;
        if (wavetimer > 0)
            return;

        wavetimer = ConstantsGameBalance.EnemyWaveEveryTicks;


        Vector2 waveOrigin;
        do
        {
            waveOrigin = new Vector2(Random.Range(-1, 1f), Random.Range(-1, 1f));
        } while (waveOrigin == Vector2.zero);
        waveOrigin = waveOrigin.normalized * ConstantsGameBalance.EnemySpawnDistance;



        float EnemiesToSpawn = numOfPlayerStructs * ConstantsGameBalance.EnemiesPerStructures;
        StartCoroutine(SpawnWave((int)EnemiesToSpawn, waveOrigin));

    }

    private void SpawnRandomEnemies(int numOfPlayerStructs)
    {

        randomsEnemyCounter += numOfPlayerStructs * Map.gameTickTime * ConstantsGameBalance.RandomEnemiesPerStructuresPerSecond;
        while (randomsEnemyCounter > 1)
        {

            randomsEnemyCounter--;
            Vector2 spawnlocation;
            do
            {
                spawnlocation = new Vector2(Random.Range(-1, 1f), Random.Range(-1, 1f));
            } while (spawnlocation == Vector2.zero);
            spawnlocation = spawnlocation.normalized * ConstantsGameBalance.EnemySpawnDistance;


            Map.Instance.AddStructure(new EnemyShip(spawnlocation));
        }
    }

    private IEnumerator SpawnWave(int enemiesToSpawn, Vector2 waveOrigin)
    {
        float spaceBetweenEnemies = 0.5f;
        int rows = (int)System.Math.Sqrt(enemiesToSpawn);
        int columns = rows == 0 ? 0 : enemiesToSpawn / rows;

        for (int rowcount = 0; rowcount < rows; rowcount++)
        {
            for (int colcount = 0; colcount < columns; colcount++)
            {
                Vector2 offset = new Vector2(rowcount * spaceBetweenEnemies, colcount * spaceBetweenEnemies);
                yield return new WaitForSeconds(0.02f);
                Map.Instance.AddStructure(new EnemyShip(waveOrigin + offset));
                enemiesToSpawn -= 1;
            }
        }

        int remainingenemies = enemiesToSpawn;
        float baseRowOffset = (columns / 2) + (enemiesToSpawn * spaceBetweenEnemies) / 2;
        for (int i = 0; i < remainingenemies; i++)
        {
            Vector2 offset = new Vector2(-1 * spaceBetweenEnemies, baseRowOffset + i * spaceBetweenEnemies);
            yield return new WaitForSeconds(0.02f);
            Map.Instance.AddStructure(new EnemyShip(waveOrigin + offset));

        }
    }
}
