﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public static CameraScript MainCamera;

    public float cameraMovementKeybSensitivity = 30; // units / second
    public float cameraMovementSensitivity = 20; // units / second
    public float cameraZoomSpeed = 0.5f;

    public float minZoomLevel = 2; // 2 is good
    public float maxZoomLevel    = 30; // 30 is also good

    // only used for fade to black. Optional.
    public UnityEngine.UI.RawImage fadeToBlackPicture;


    // this is camera ortographic size, basically
    public float zoomLevel = 4;
    public Vector2 center = new Vector2(0,0);

    bool animating = false;

    // Use this for initialization
    void Awake () {
        if (MainCamera != null)
            throw new System.InvalidOperationException("Camera instance is already present!");

        MainCamera = this;

        

	}
	
	// Update is called once per frame
	void Update () {
        var camera = this.GetComponent<Camera>();
       
        camera.transform.localPosition = new Vector3(center.x, center.y, -10);
        camera.orthographicSize = zoomLevel;


        if (animating)
            return;

        // zoom
        if (Input.mouseScrollDelta.y > 0.1 || Input.GetKey(KeyCode.KeypadMinus))
        {
            zoomLevel -= cameraZoomSpeed;
        }
        else if (Input.mouseScrollDelta.y < -0.1 || Input.GetKey(KeyCode.KeypadPlus))
        {
            zoomLevel += cameraZoomSpeed;
        }


        if (zoomLevel < minZoomLevel)
            zoomLevel = minZoomLevel;
        if (zoomLevel > maxZoomLevel)
            zoomLevel = maxZoomLevel;

        // keyboard movement
        if (Input.GetKey(KeyCode.S))
        {
            center += new Vector2(0, -cameraMovementKeybSensitivity * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.W))
        {
            center += new Vector2(0, cameraMovementKeybSensitivity * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            center += new Vector2(cameraMovementKeybSensitivity * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            center += new Vector2(-cameraMovementKeybSensitivity * Time.deltaTime, 0);
        }


        Vector3 mouse = camera.ScreenToViewportPoint(Input.mousePosition);
        
        if (mouse.y < 0.1)
        {
            center += new Vector2(0, -cameraMovementSensitivity * Time.deltaTime);
        }
        if (mouse.y > 0.9)
        {
            center += new Vector2(0, cameraMovementSensitivity * Time.deltaTime);
        }
        if (mouse.x > 0.9)
        {
            center += new Vector2(cameraMovementSensitivity * Time.deltaTime, 0);
        }
        if (mouse.x < 0.1)
        {
            center += new Vector2(-cameraMovementSensitivity * Time.deltaTime, 0);
        }


    }


    public void StartMoveAndCenterOn(Vector2 pos, float zoom, float time)
    {
        StartCoroutine(MoveAndCenterOn(pos, zoom, time));
    }

    internal IEnumerator MoveAndCenterOn(Vector2 position, float zoom, float time)
    {
        animating = true;
        Vector3 originalPos = this.center;
        float originalzoom = this.zoomLevel;
        Vector3 newPos = new Vector3(position.x, position.y, originalPos.z);
        float start = Time.time;
        while (Time.time - start < time)
        {
            float lerping = (Time.time - start) / time;

            center = Vector3.Lerp(originalPos, newPos, lerping);
            this.zoomLevel = originalzoom * (1 - lerping) + zoom * lerping;
            yield return null;
        }
        center = newPos;
        this.zoomLevel = zoom;
        animating = false;
    }


    // TODOLOW: copy and paste FTW
    internal IEnumerator ZoomInAndFade(Vector2 position, float stopZoom, float time)
    {
        this.minZoomLevel = 0;

        animating = true;
        Vector3 originalPos = this.center;
        float originalzoom = this.zoomLevel;
        Vector3 newPos = new Vector3(position.x, position.y, originalPos.z);
        float start = Time.time;
        while (Time.time - start < time)
        {
            float lerping = (Time.time - start) / time;

            center = Vector3.Lerp(originalPos, newPos, lerping);
            this.zoomLevel = originalzoom * (1 - lerping) + stopZoom * lerping;

            if (fadeToBlackPicture != null)
                fadeToBlackPicture.color = new Color(0, 0, 0, lerping);
            yield return null;
        }
        center = newPos;
        this.zoomLevel = stopZoom;
        animating = false;

        
    }
}
