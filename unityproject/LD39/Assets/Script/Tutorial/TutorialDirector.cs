﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TutorialDirector : MonoBehaviour {

    public TextMesh textForTutorial;
    public GameObject arrowSelector;
    public Camera camera;

    public InputField text;
    public Canvas UILayout;

    // Use this for initialization
    void Start () {
        StartCoroutine(MainMenu());
    }

    private IEnumerator MainMenu()
    {
        yield return MoveCameraTo(0, -40);

        int selection = 0;
        int maxOptionId= 1;

        while (!Input.GetKeyDown(KeyCode.Return))
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) && selection > 0)
                selection--;
            if (Input.GetKeyDown(KeyCode.DownArrow) && selection < maxOptionId)
                selection++;
            arrowSelector.transform.position = new Vector2(-0.1f, -40 - selection);
            yield return null;
        }

        if (selection == 0)
            yield return StartCoroutine(tutorialCoroutine());
        else if (selection == 1)
            Application.Quit();
        else
            Application.Quit();
    }

    bool nameEdited = false;
    public void EndEditName()
    {
        nameEdited = true;
    }


    IEnumerator tutorialCoroutine()
    {

        
        yield return MoveCameraTo(0, 0);
        PrintThis("Enter your name and press ENTER", new Vector2(0, 0));
        UILayout.enabled = true;
        text.enabled = true;
        text.text = Assets.Script.Scorekeeping.TopScores.Name;
        text.Select();
        text.ActivateInputField();
        while (!nameEdited)
            yield return null;

        Assets.Script.Scorekeeping.TopScores.Name = text.text;
        UILayout.enabled = false;
        text.enabled = false;
        
        PrintThis(String.Format("Welcome {0} :)\n\n", text.text) +
            "for any message in this introduction, press any key to continue.\n\n"+
            "If you want to jump straight in the game press ESC *NOW* to skip (it won't work after this message)", new Vector2(0, 0));

        yield return MoveCameraTo(0, 0);
        yield return StartCoroutine(WaitForKey());
        if (Input.GetKey(KeyCode.Escape))
            goto finish;
        
        PrintThis("3172 AD: Humans have gone well beyond earth\nand started colonizing entire galaxies.", new Vector2(0, 0));
        yield return StartCoroutine(WaitForKey());
        PrintThis("Intergalactic travel still posed some risks, so humans started using entirely\n automated stations for mining expeditions...", new Vector2(0, 0));
        yield return StartCoroutine(WaitForKey());

        yield return StartCoroutine(MoveCameraTo(0, 10));
        PrintThis("Bases like these ones.", new Vector2(0, 10), TextAlignment.Left);
        yield return StartCoroutine(WaitForKey());
        PrintThis("You MUST keep it powered, or we will lose contact with it.", new Vector2(0, 10), TextAlignment.Left);
        yield return StartCoroutine(WaitForKey());


        yield return StartCoroutine(MoveCameraTo(0, 20));
        PrintThis("These are two of the common asteroids that can be found in the galaxy\nLet's examine them in more details.", new Vector2(0, 20), TextAlignment.Center);
        yield return StartCoroutine(WaitForKey());
        PrintThis("This is an asteroid full of radioactive materials. \nThey can be used to generate energy even in absence of a sun.", new Vector2(0.5f, 20+2), TextAlignment.Left);
        yield return StartCoroutine(WaitForKey());
        PrintThis("This is an asteroid full of precious metals.\nThese metals can be used for construction on the spot,\nand the surplus will be sent automatically to some friendly colony.", new Vector2(0.5f, 20-2), TextAlignment.Left);
        yield return StartCoroutine(WaitForKey());
        
        yield return StartCoroutine(MoveCameraTo(0, 30));
        PrintThis("Just build a MINER next to them to automatically extract the resources over time!", new Vector2(0, 30));
        yield return StartCoroutine(WaitForKey());
        PrintThis("But you are not the only one wanting these resources.", new Vector2(0, 30));
        yield return StartCoroutine(WaitForKey());

        yield return StartCoroutine(MoveCameraTo(0, 40));
        PrintThis("Aliens will try to stop the mining operation at all costs.", new Vector2(0, 40));
        yield return StartCoroutine(WaitForKey());
        PrintThis("The more your base grows in size,\nthe stronger the attacks will become.", new Vector2(0, 40));
        yield return StartCoroutine(WaitForKey());
        PrintThis("For defending you can build some cannons, even though...\n", new Vector2(0, 40));
        yield return StartCoroutine(WaitForKey());
        PrintThis("...at some point they will surely overrun your base.\n\nYour objective is to \nTAKE AS MUCH METAL HOME AS POSSIBLE\nand\nKILL AS MANY ALIENS AS YOU CAN\n\nGood Luck!", new Vector2(0, 40));
        yield return StartCoroutine(WaitForKey());
        finish:

        yield return StartCoroutine(MoveCameraTo(0, 50));
        PrintThis("CONTROLS:\nuse the WASD keys to move the camera around the screen (also the mouse is ok). \nMouse wheel zooms in/out\nNumbers (on top of WASD) to select what to build (list of structures will be at the top left of the screen).", new Vector2(0, 50));
        
        yield return StartCoroutine(WaitForKey());

        SceneManager.LoadScene("game");
        yield break;
    }

    private IEnumerator MoveCameraTo(int x, int y)
    {
        if (camera == null)
            yield break;
        Vector3 originalPos = camera.transform.position;
        Vector3 newPos = new Vector3(x, y, originalPos.z);
        float start = Time.time;
        while (Time.time - start < 1)
        {
            camera.transform.position = Vector3.Lerp(originalPos, newPos, Time.time - start);
            yield return null;
#if DEBUG
            if (Input.anyKeyDown)
                break;
#endif
        }
        camera.transform.position = newPos;
    }

    private void PrintThis(string text, Vector2 pos, TextAlignment align = TextAlignment.Center)
    {
        textForTutorial.text = text;
        switch (align)
        {
            case TextAlignment.Center: textForTutorial.anchor = TextAnchor.MiddleCenter; break;
            case TextAlignment.Left: textForTutorial.anchor = TextAnchor.UpperLeft; break;
            case TextAlignment.Right: textForTutorial.anchor = TextAnchor.UpperRight; break;
        }
        textForTutorial.alignment = align;
        textForTutorial.transform.position = pos;
    }

    private IEnumerator WaitForKey()
    {
        yield return null;
        while (!Input.anyKeyDown)
        {
            yield return null;
        }
    }
}
