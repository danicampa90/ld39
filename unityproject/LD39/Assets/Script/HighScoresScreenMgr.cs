﻿using Assets.Script.Scorekeeping;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HighScoresScreenMgr : MonoBehaviour {

    public TextMesh UInames;
    public TextMesh UInumbers;
    public TextMesh UIyou;

    // Use this for initialization
    void Start () {
        string names="(Scores Not available!)", scores="";
        if (TopScores.Scores != null)
        {
            names = "";
            for (int i = 0; i < 10 && i < TopScores.Scores.Length; i++)
            {
                names += TopScores.Scores[i].name + "\n";
                scores += TopScores.Scores[i].score + "\n";
            }
        }
        UInames.text = names;
        UInumbers.text = scores;
        UIyou.text = string.Format("You rank globally as {0}th with {1} pts!", TopScores.myPosition, TopScores.myScore.score);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown)
        {
            Map.Instance = null;
            CameraScript.MainCamera = null;
            TopScores.Reset();
            SceneManager.LoadScene(0);
        }
	}
}
