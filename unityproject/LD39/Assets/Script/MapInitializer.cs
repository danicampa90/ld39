﻿using Assets.Script.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapInitializer : MonoBehaviour {

    class XorShifhtRandom
    {
        protected uint _x = 123456789;
        protected uint _y = 362436069;
        protected uint _z = 521288629;
        protected uint _w = 88675123;

        protected void XorShiftFillBuffer(byte[] buf, int offset, int offsetEnd)
        {
            while (offset < offsetEnd)
            {
                uint t = _x ^ (_x << 11);
                _x = _y; _y = _z; _z = _w;
                _w = _w ^ (_w >> 19) ^ (t ^ (t >> 8));
                buf[offset++] = (byte)(_w & 0xFF);
                buf[offset++] = (byte)((_w >> 8) & 0xFF);
                buf[offset++] = (byte)((_w >> 16) & 0xFF);
                buf[offset++] = (byte)((_w >> 24) & 0xFF);
            }
        }
        public float NextFloat()
        {
            byte[] buff = new byte[sizeof(float)];
            XorShiftFillBuffer(buff, 0, buff.Length);
            return System.BitConverter.ToSingle(buff, 0);
        }
        public int NextInt()
        {
            byte[] buff = new byte[sizeof(float)];
            XorShiftFillBuffer(buff, 0, buff.Length);
            return System.BitConverter.ToInt32(buff, 0);
        }
    }


    private void Start()
    {
        System.Random rnd = new System.Random();

        //long seed = System.DateTime.Now.Ticks;

        for (int i = 0; i < ConstantsGameBalance.AsteroidMetalCount; i++)
        {
            againMetal:
            float x = (float)(rnd.NextDouble() * 2 - 1) * ConstantsGameBalance.MapSize;
            float y = (float)(rnd.NextDouble() * 2 - 1) * ConstantsGameBalance.MapSize;
            if (new Vector2(x, y).magnitude > ConstantsGameBalance.MapSize)
                goto againMetal;
            int size = rnd.Next(ConstantsGameBalance.AsteroidMetalMin, ConstantsGameBalance.AsteroidMetalMax);
            Map.Instance.AddStructure(new MetalAsteroid(new Vector2(x, y), size));
        }

        for (int i = 0; i < ConstantsGameBalance.AsteroidEnergyCount; i++)
        {
            againEnergy:
            float x = (float)(rnd.NextDouble() * 2 - 1) * ConstantsGameBalance.MapSize;
            float y = (float)(rnd.NextDouble() * 2 - 1) * ConstantsGameBalance.MapSize;
            if (new Vector2(x, y).magnitude > ConstantsGameBalance.MapSize)
                goto againEnergy;
            int size = rnd.Next(ConstantsGameBalance.AsteroidEnergyMin, ConstantsGameBalance.AsteroidEnergyMax);
            Map.Instance.AddStructure(new EnergyAsteroid(new Vector2(x, y), size));
        }

        var nearestAsteroid = Map.Instance.GetNearestStructureOfType<EnergyAsteroid>(Vector2.zero);
        Vector2 baseInitialPosition = nearestAsteroid.Position + new Vector2(2, 2);

        Map.Instance.AddStructure(new Base(baseInitialPosition));


        CameraScript.MainCamera.zoomLevel = 30;
        CameraScript.MainCamera.center = baseInitialPosition;
        CameraScript.MainCamera.StartMoveAndCenterOn(baseInitialPosition, 6, 1);
    }



}
