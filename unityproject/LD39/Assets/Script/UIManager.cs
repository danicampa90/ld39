﻿using Assets.Script.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIManager : MonoBehaviour
{

    public GhostCursorScript cursor;
    public Text structureHelpUIText;
    public Text selectionUIText;
    public Text metalEnergyMeters;
    public Text scoreUIText;

    public AudioClip confirmSound;
    public AudioClip denySound;

    StructureDB.StructureBuildInfo selectedStructure;
    

    // Use this for initialization
    void Start()
    {
        structureHelpUIText.text += "\n";
        foreach (var strInfo in StructureDB.keymap)
        {
            var keyname = strInfo.Key.ToString().Replace("Alpha", "Number ");
            structureHelpUIText.text += keyname + " : " + strInfo.Value.name + " ( M: " + strInfo.Value.metalcost + " - E: " + strInfo.Value.energycost.ToString() + ")\n";
        }
        selectedStructure = null;
        UpdateUI();

        Map.Instance.OnTick += Instance_OnTick;
    }

    private void Instance_OnTick()
    {
        UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {

        var mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        mousePosition.z = 0;

        if (Input.GetMouseButtonDown(0) && selectedStructure != null)
        {
            IStructure nearestStructure = Map.Instance.GetNearestStructureOfType<IStructure>(mousePosition);
            float distanceToNearestStructure = (nearestStructure.Position - (Vector2)mousePosition).magnitude;
            if (!selectedStructure.CanBeBuilt() || distanceToNearestStructure < ConstantsGameBalance.StructuresMinDistances)
            {
                GetComponent<AudioSource>().clip = denySound;
                GetComponent<AudioSource>().Play();
                return;
            }
            Map.Instance.Energy -= selectedStructure.energycost;
            Map.Instance.Metal-= selectedStructure.metalcost;
            IStructure str = selectedStructure.factory(mousePosition);
            Map.Instance.AddStructure(str);

            selectedStructure = null;

            GetComponent<AudioSource>().clip = confirmSound;
            GetComponent<AudioSource>().Play();

            UpdateUI();
        }
        if(selectedStructure != null && cursor != null)
        {
            cursor.MoveTo(mousePosition);
        }
        foreach (var keymapStructInfo in StructureDB.keymap)
        {
            if (Input.GetKeyDown(keymapStructInfo.Key))
            {
                selectedStructure = keymapStructInfo.Value;
                UpdateUI();
            }
        }
        

    }

    private void UpdateUI()
    {
        if (selectedStructure == null)
        {
            this.selectionUIText.text = "";
            cursor.Hide();
        }
        else
        {
            this.selectionUIText.text = String.Format("Placing a {0}\n Costs {1} metal, {2} energy.", selectedStructure.name, selectedStructure.metalcost, selectedStructure.energycost);
            cursor.SetRange(this.selectedStructure.range);
        }
        if(Map.Instance != null && this.metalEnergyMeters != null)
            this.metalEnergyMeters.text = String.Format("Energy: {0} / {1}\nMetal: {2} / {3}", Map.Instance.Energy, Map.Instance.EnergyMax, Map.Instance.Metal, Map.Instance.MetalMax);
        if(Map.Instance != null && this.scoreUIText != null)
        {
            this.scoreUIText.text = String.Format("{0} Metal Sent\n{1} Enemies Killed\n{2} Total score", Map.Instance.MetalSentHome, Map.Instance.EnemiesKilled, Map.Instance.TotalScore);
        }
    }
}
