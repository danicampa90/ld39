﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostCursorScript : MonoBehaviour {

    public SpriteRenderer rangeDiag;

    public void MoveTo(Vector2 position)
    {
        if (!this.GetComponent<Renderer>().enabled)
        {
            this.GetComponent<Renderer>().enabled = true;
            rangeDiag.GetComponent<Renderer>().enabled = true;
        }
        this.transform.position = position;
    }

    public void SetRange(float range)
    {
        float scale = (range / this.rangeDiag.size.x) *2;
        rangeDiag.transform.localScale = new Vector2(scale, scale);
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    internal void Hide()
    {
        this.GetComponent<Renderer>().enabled = false;
        rangeDiag.GetComponent<Renderer>().enabled = false;
    }
}
