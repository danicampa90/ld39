﻿using Assets.Script.Model;
using Assets.Script.Scorekeeping;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour {

    public const float gameTickTime = 0.1f;
    public static Map Instance;

    // resources
    public int HiddenBaseEnergy = 20;
    public int HiddenBaseEnergyMax = 0;
    public int Energy = 100;
    public int EnergyMax = 100;
    public int Metal = 100;
    public int MetalMax = 100;

    // score
    public bool gameover = false;
    public int MetalSentHome = 0;
    public int EnemiesKilled = 0;
    public int TotalScore { get { return EnemiesKilled * 10 + MetalSentHome * 2; } }


    // external tick handlers (not structures).
    public delegate void OnTickEventHandler();
    public delegate void OnExplosionEventHandler(Vector2 pos, float size);
    public event OnTickEventHandler OnTick;
    public event OnExplosionEventHandler OnExplosion;

    // list of structures. NB: includes also enemies, even though they are not structures (-.-)
    public List<IStructure> structures = new List<IStructure>();

    public T GetNearestStructureOfType<T>(Vector2 position) where T:IStructure {
        float nearestDistance = float.PositiveInfinity;
        T nearest = default(T);

        foreach (var str in structures)
        {
            float distance = (position - str.Position).sqrMagnitude;
            if(distance < nearestDistance && str is T)
            {
                nearest = (T)str;
                nearestDistance = distance;
            }
        }

        return nearest;
    }



    public IEnumerable<T> GetStructuresInRange<T>(Vector2 position, float range) where T : IStructure
    {
        foreach (var str in structures)
        {
            float distance = (position - str.Position).magnitude;
            if (distance < range && !str.Dead && str is T)
            {
                yield return (T) str;
            }
        }

        yield break;
    }

    internal void AddStructure(IStructure str)
    {
        if (str.Dead)
            return;
        GameObject prefab = null;
        try
        {
            prefab = Instantiate(Resources.Load<GameObject>(str.PrefabName));
        }
        catch { prefab = Instantiate(Resources.Load<GameObject>(ConstantsPrefabNames.Unknown)); }

        prefab.transform.SetParent(this.transform);
        prefab.transform.position = str.Position;
        PrefabLogicScript scr = prefab.GetComponent<PrefabLogicScript>();

        if (scr == null)
        {
            // add the default script for the components that have no custom behavior.
            // it handles the "die" mechanism
            scr = prefab.AddComponent<DefaultPrefabBehavior>();
        }

        scr.SetModelStructure(str);
        structures.Add(str);
        
    }


    // Use this for initialization
    void Awake () {

        if (this.gameover)
            return;

        if (Instance != null)
            throw new InvalidOperationException("Map instance is already present!");

        Instance = this;

    }

    private void Start()
    {

        if (this.gameover)
            return;
        StartCoroutine(DoGameTick());
    }

    public static void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    private IEnumerator DoGameTick()
    {
        if (this.gameover)
            yield break;
        while (true)
        {
            yield return new WaitForSeconds(gameTickTime);

            if (gameover)
                yield break;

            int playerStructureCount = 0;
            List<IStructure> randomList = new List<IStructure>(structures);
            Shuffle(randomList);
            foreach (var str in randomList)
            {
                str.Tick();
                if (str is IPlayerStructure)
                    playerStructureCount++;
            }


            this.MetalMax = 90 + playerStructureCount * 10;
            this.EnergyMax = 80 + playerStructureCount * 20;
            // send home extra metal
            if (Metal > MetalMax)
            {
                this.MetalSentHome += Metal - MetalMax;
                Metal = MetalMax;
            }

            if (gameover)
            {
                //if we lost this tick, don't remove the structures that died, otherwise we would remove the base, too
                break;
            }

            foreach (var str in structures)
            {
                if (str.Exploded && str.Dead)
                    this.OnExplosion(str.Position, 1);
            }

            // remove dead people
            structures.RemoveAll((IStructure str) => str.Dead);
            if (OnTick != null)
                OnTick();
        }

        // the only way we end up here is via losing the game.

        Base b = GetNearestStructureOfType<Base>(new Vector2(0, 0)); // Inefficient, but who cares.
        if (b != null)
        {
            Score score = new Score();
            score.name = TopScores.Name;
            score.metal = MetalSentHome;
            score.kills = EnemiesKilled;
            score.score = TotalScore;

            bool finishedSending = false;

            GameObject submitter = new GameObject("ScoreSubmitter");
            submitter.AddComponent<ScoreAPI>().SendScores(score, () => { finishedSending = true; }, ()=> { finishedSending = true; });
            // move there
            yield return StartCoroutine(CameraScript.MainCamera.MoveAndCenterOn(b.Position, 6, 2));
            yield return StartCoroutine(CameraScript.MainCamera.MoveAndCenterOn(b.Position, 20, 5));
            // and ZOOM!
            yield return StartCoroutine(CameraScript.MainCamera.ZoomInAndFade(b.Position, 0.1f, 1f));

            while(finishedSending == false)
                yield return new WaitForSeconds(gameTickTime);

            // THE END!
            UnityEngine.SceneManagement.SceneManager.LoadScene("stats");

        }
    }


    internal void LoseGame()
    {
        this.gameover = true;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    
}
