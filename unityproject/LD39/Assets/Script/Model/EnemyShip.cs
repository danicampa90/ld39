﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.Model
{
    class EnemyShip : StructureBase, IStructure, IEnemyTarget
    {
        int scramblingAwayTimer = 0;
        Vector2 scramblingDirection;

        public EnemyShip(Vector2 position) : base(position)
        {
        }

        public override string PrefabName
        {
            get
            {
                return "EnemyShip";
            }
        }

        public void Kill()
        {
            Die(false);
        }

        public override void Tick()
        {
            if (scramblingAwayTimer > 0)
            {
                scramblingAwayTimer--;
                this.Position += scramblingDirection * ConstantsGameBalance.EnemySpeed * Map.gameTickTime;
                CallOnChange();
                return;
            }
            var playerStruct = Map.Instance.GetNearestStructureOfType<IPlayerStructure>(this.Position);
            if (playerStruct != null)
            {
                var direction = (playerStruct.Position - this.Position);
                if (direction.magnitude > ConstantsGameBalance.EnemySpeed * Map.gameTickTime)
                    direction = direction.normalized * ConstantsGameBalance.EnemySpeed * Map.gameTickTime;
                
                else
                {
                    playerStruct.Kill();
                    scramblingAwayTimer = ConstantsGameBalance.EnemyScrambleAwayTimer;


                    var x = UnityEngine.Random.Range(-1f, 1f);
                    var y = UnityEngine.Random.Range(-1f, 1f);
                    scramblingDirection = new Vector2(x, y).normalized;
                }
                this.Position = this.Position + direction;
                this.CallOnChange();
            }
        }
    }
}
