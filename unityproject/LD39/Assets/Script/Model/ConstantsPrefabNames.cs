﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Script.Model
{
    class ConstantsPrefabNames
    {
        // debug only
        public const string Unknown = "UnknownObject";

        // real structures
        public const string MetalAsteroid = "Asteroid.Metal";
        public const string EnergyAsteroid = "Asteroid.Energy";
        public const string StructureMiner = "Structure.Miner";
        public const string StructureShooter = "Structure.Shooter";
        public const string Base = "Structure.Base";

        public const string Explosion = "Explosion";
    }
}
