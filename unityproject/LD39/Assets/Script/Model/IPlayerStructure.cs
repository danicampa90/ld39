﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Script.Model
{
    interface IPlayerStructure : IStructure
    {
        void Kill();
    }
}
