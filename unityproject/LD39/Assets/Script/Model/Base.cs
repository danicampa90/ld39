﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.Model
{
    class Base : StructureBase, IStructure, IPlayerStructure
    {
        int baseEnergyConsumptionTimer = 0;

        public Base(Vector2 position) : base(position)
        {

        }

        public override string PrefabName
        {
            get
            {
                return ConstantsPrefabNames.Base;
            }
        }

        public void Kill()
        {
            Map.Instance.LoseGame();
            // don't remove the graphics because we want to show the cool animation at the end.
            //Die();
        }
        

        public override void Tick()
        {

            if (Map.Instance.HiddenBaseEnergy < Map.Instance.HiddenBaseEnergyMax && Map.Instance.Energy > 0)
            {
                int toTransfer = Map.Instance.HiddenBaseEnergyMax - Map.Instance.HiddenBaseEnergy;
                if (toTransfer > Map.Instance.Energy)
                    toTransfer = Map.Instance.Energy;
                Map.Instance.HiddenBaseEnergyMax += toTransfer;
                Map.Instance.Energy -= toTransfer;
            }

            if(baseEnergyConsumptionTimer < ConstantsGameBalance.BaseEnergyConsumptionEveryTicks)
            {
                baseEnergyConsumptionTimer++;
                return; // do not consume power
            } else
            {
                // reset timer and continue
                baseEnergyConsumptionTimer = 0;
            }

            if (Map.Instance.Energy > 0)
            {
                Map.Instance.Energy--;
            }
            else if (Map.Instance.HiddenBaseEnergy > 0)
            {
                Map.Instance.HiddenBaseEnergy--;
            }
            else
            {
                Kill();
            }

        }
    }
}
