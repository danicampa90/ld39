﻿
using UnityEngine;


namespace Assets.Script.Model
{

    public delegate void OnStructureModelChange(IStructure structure);

    // NB: need to override ToString to get the name to show to the user.
    public interface IStructure
    {
        bool Dead { get; }
        bool Exploded { get; } // Dead must be true. The structure will be replaced with an explosion.
        Vector2 Position { get; }
        string PrefabName { get; }
        void Tick();

        event OnStructureModelChange OnChange;
    }
}