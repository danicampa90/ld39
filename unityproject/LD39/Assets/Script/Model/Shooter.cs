﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.Model
{
    class Shooter : StructureBase, IPlayerStructure, IStructure
    {
        public IEnemyTarget target;
        int reloadCooldown;

        public Shooter(Vector2 position) : base(position)
        {

        }

        public override string PrefabName
        {
            get
            {
                return ConstantsPrefabNames.StructureShooter;
            }
        }

        public void Kill()
        {
            Die(true);
        }

        public override void Tick()
        {
            if(target != null && (Position - target.Position).magnitude < ConstantsGameBalance.ShooterRange)
            {
                // this dies. animation should have started the tick before, so at this time it dies.
                target = null;
                CallOnChange();
            }

            if(reloadCooldown > 0)
            {
                reloadCooldown--;
                return;
            }
            var map = Map.Instance;
            // if we are out of power don't even try
            if (map.Energy == 0)
            {
                return;
            }

            // find nearest enemy
            var enemies = map.GetStructuresInRange<IEnemyTarget>(this.Position, ConstantsGameBalance.ShooterRange);
            float minimum = float.PositiveInfinity;
            IEnemyTarget nearestTarget = null;

            foreach (var enemy in enemies)
            {
                var distance = (Position - enemy.Position).sqrMagnitude;
                if (distance < minimum) {
                    nearestTarget = enemy;
                    minimum = distance;
                }
            }

            // consume energy if we are trying to shoot something
            if (nearestTarget != null)
            {
                if (map.Energy < ConstantsGameBalance.EnergyPerBullet)
                {
                    return;
                }
                map.Energy -= ConstantsGameBalance.EnergyPerBullet;
            }

            // now set the info for the rendering and kill the target
            this.target = nearestTarget;
            if (target != null)
            {
                target.Kill();
                Map.Instance.EnemiesKilled++;
            }

            CallOnChange();

            // wait reload before shooting again
            this.reloadCooldown = ConstantsGameBalance.ShooterReloadTimeTicks;

        }
    }
}
