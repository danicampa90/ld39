﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.Model
{
    class Miner : StructureBase, IStructure, IPlayerStructure
    {

        
        int slowMiningOpTimer = ConstantsGameBalance.MinerOperationEveryTicks;
        public IMinable target = null;

        public Miner(Vector2 position) : base(position)
        {
            IMinable nearestMetal, nearestEnergy;
            int availableEnergy, availableMetal;

            GetAvailableResources(out nearestMetal, out nearestEnergy, out availableEnergy, out availableMetal);
            if(availableMetal+availableEnergy == 0)
            {
                this.Die(false);
                StructureDB.Refund(this);
            }
        }

        public override string PrefabName
        {
            get
            {
                return ConstantsPrefabNames.StructureMiner;
            }
        }

        public void Kill()
        {
            Die(true);
        }

        public override void Tick()
        {
            if(target != null)
            {
                target = null;
                CallOnChange();
            }
            if (slowMiningOpTimer > 0)
            {
                slowMiningOpTimer--;
                return;
            }
            var map = Map.Instance;
            IMinable nearestMetal, nearestEnergy;
            int availableEnergy, availableMetal;

            GetAvailableResources(out nearestMetal, out nearestEnergy, out availableEnergy, out availableMetal);

            if (availableEnergy == 0 && availableMetal == 0 && !Dead)
            {
                StructureDB.Refund(this, 0.5f);
                Die(false);
            }
            // now, we need to perform decisions

            int energyToMine = map.EnergyMax - map.Energy;
            // if we are not full of energy and we have energy nearby
            bool mineEnergy = energyToMine >= ConstantsGameBalance.MinerBudgetPerOp && availableEnergy > 0;
            bool minedSomething = false;

            if (mineEnergy && nearestEnergy != null)
            {
                // cap the energy
                if (energyToMine > ConstantsGameBalance.MinerBudgetPerOp)
                    energyToMine = ConstantsGameBalance.MinerBudgetPerOp;

                int minedenergy = nearestEnergy.MineEnergy(energyToMine);
                if (minedenergy != 0)
                {
                    minedSomething = true;
                    target = nearestEnergy;
                }
                map.Energy += minedenergy;
            }
            else if (nearestMetal != null && map.Energy > ConstantsGameBalance.MinerEnergyUsePerOp)
            {
                int minedmetal = nearestMetal.MineMetal(ConstantsGameBalance.MinerBudgetPerOp);
                if (minedmetal != 0)
                {
                    minedSomething = true;
                    target = nearestMetal;
                }
                map.Metal += minedmetal;

            }

            if (minedSomething)
            {
                // consume power
                map.Energy -= ConstantsGameBalance.MinerEnergyUsePerOp;
                // wait a bit for next operation
                slowMiningOpTimer = ConstantsGameBalance.MinerOperationEveryTicks;

                CallOnChange();
            }
            
        }

        private void GetAvailableResources(out IMinable nearestMetal, out IMinable nearestEnergy, out int availableEnergy, out int availableMetal)
        {
            nearestMetal = null;
            nearestEnergy = null;
            availableEnergy = 0;
            availableMetal = 0;
            var nearAsteroids = Map.Instance.GetStructuresInRange<IMinable>(Position, ConstantsGameBalance.MinerRange);
            foreach (var asteroid in nearAsteroids)
            {
                availableEnergy += asteroid.EnergyAmount;
                availableMetal += asteroid.MetalAmount;
                if (asteroid.EnergyAmount > 0)
                    nearestEnergy = asteroid; // TODOMID: an order to this would be better
                if (asteroid.MetalAmount > 0)
                    nearestMetal = asteroid; // TODOMID: an order to this would be better
            }
        }
    }
}
