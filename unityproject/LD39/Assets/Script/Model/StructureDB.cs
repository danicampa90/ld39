﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.Model
{
    public class StructureDB
    {
        public delegate IStructure StructureFactory(Vector2 position);

        public class StructureBuildInfo
        {
            public string name;
            public int metalcost;
            public int energycost;

            public float range;
            public StructureFactory factory;

            public StructureBuildInfo(string name, int energy, int metal, float range, StructureFactory factory)
            {
                this.name = name;
                this.energycost = energy;
                this.metalcost = metal;
                this.factory = factory;
                this.range = range;
            }

            internal bool CanBeBuilt()
            {
                return Map.Instance.Energy >= energycost && Map.Instance.Metal >= metalcost;
            }
        }

        public static Dictionary<KeyCode, StructureBuildInfo> keymap;

        public static List<StructureBuildInfo> database;

        static StructureDB()
        {
            database = new List<StructureBuildInfo>();
            keymap = new Dictionary<KeyCode, StructureBuildInfo>();

            database.Add(new StructureBuildInfo("miner", 0, 15, ConstantsGameBalance.MinerRange, (Vector2 x) => new Miner(x)));
            database.Add(new StructureBuildInfo("shooter", 30, 100, ConstantsGameBalance.ShooterRange, (Vector2 x) => new Shooter(x)));

#if DEBUG
            database.Add(new StructureBuildInfo("DEBUG:M-AST", 0, 0, 0, (Vector2 x) => new MetalAsteroid(x, 100)));
            database.Add(new StructureBuildInfo("DEBUG:E-AST", 0, 0, 0, (Vector2 x) => new EnergyAsteroid(x, 100)));
            database.Add(new StructureBuildInfo("DEBUG:ENEMY", 0, 0, 0, (Vector2 x) => new EnemyShip(x)));
#endif

            keymap = new Dictionary<KeyCode, StructureBuildInfo>();
            var fnkeys = new List<KeyCode>() { KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8 };

            for (int i = 0; i < database.Count; i++)
            {
                keymap.Add(fnkeys[i], database[i]);
            }

        }


        internal static void Refund(IStructure structureOnMap, float multiplier = 1)
        {
            string structurename = null;
            if (structureOnMap is Miner)
                structurename = "miner";
            if (structureOnMap is Shooter)
                structurename = "shooter";
            try
            {
                var structinfo = database.Find((s) => s.name == structurename);
                Map.Instance.Metal += (int)(structinfo.metalcost * multiplier);
                Map.Instance.Energy += (int)(structinfo.energycost * multiplier);
            }
            catch { /* not found? */ return; }
        }


    }
}
