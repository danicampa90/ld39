﻿using UnityEngine;

namespace Assets.Script.Model
{
    internal class ConstantsGameBalance
    {
        public const float MinerRange = 2;
        public const int MinerBudgetPerOp = 15;
        public const int MinerEnergyUsePerOp = 1;
        public const int MinerOperationEveryTicks = 50;
        public const float EnemySpeed = 1; // units/sec
        public const int EnemyScrambleAwayTimer = 20; // ticks
        public const float ShooterRange = 5;
        public const int ShooterReloadTimeTicks = 10;
        public const int BaseEnergyConsumptionEveryTicks = 10;

        internal static readonly int AsteroidMetalMax = 400;
        internal static readonly int AsteroidMetalMin = 200;
        internal static readonly int AsteroidMetalCount = 300;
        internal static readonly int AsteroidEnergyCount = 100;
        internal static readonly int AsteroidEnergyMax = 1000;
        internal static readonly int AsteroidEnergyMin = 400;
        internal static readonly int EnergyPerBullet = 2;
        internal static readonly int EnemyStartAtStructureNumber = 0;
        internal static readonly float StructuresMinDistances=0.5f;


        internal static readonly float EnemiesPerStructures = 0.2f;
        public const int EnemyWaveEveryTicks = 200; // 20 seconds
        public const float EnemySpawnDistance = 70;
        internal static readonly float RandomEnemiesPerStructuresPerSecond = 1/10f;
        internal static readonly float MapSize=50;
    }
}