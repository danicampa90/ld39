﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.Model
{
    class EnergyAsteroid: AsteroidBase
    {
        public EnergyAsteroid(Vector2 position, int energyAmount)
            : base(position, energyAmount, 0)
        {

        }

        public override string PrefabName
        {
            get
            {
                return ConstantsPrefabNames.EnergyAsteroid;
            }
        }
    }
}
