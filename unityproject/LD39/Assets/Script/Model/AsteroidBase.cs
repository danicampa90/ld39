﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.Model
{
    abstract class AsteroidBase : StructureBase, IMinable
    {
        public int EnergyAmount
        {
            get; private set;
        }
        
        public int MetalAmount
        {
            get; private set;
        }
        
        public AsteroidBase(Vector2 position, int EnergyAmount, int MetalAmount)
            : base(position)
        {
            this.EnergyAmount = EnergyAmount;
            this.MetalAmount = MetalAmount;
        }


        public int MineEnergy(int requested)
        {
            if (requested > EnergyAmount)
                requested = EnergyAmount;
            if (requested == 0)
                return 0;
            EnergyAmount -= requested;
            CallOnChange();
            return requested;
        }

        public int MineMetal(int requested)
        {
            if (requested > MetalAmount)
                requested = MetalAmount;
            if (requested == 0)
                return 0;
            MetalAmount -= requested;

            CallOnChange();
            return requested;
        }

        public override void Tick()
        {
            if (Dead)
                return;
            if (EnergyAmount == 0 && MetalAmount == 0)
                 Die(false);
        }
    }
}
