﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Script.Model
{
    interface IMinable : IStructure
    {
        int EnergyAmount { get; }
        int MetalAmount { get; }
        int MineEnergy(int requested); // returns the amount actually mined;
        int MineMetal(int requested); // returns the amount actually mined;
    }
}
