﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.Model
{
    class MetalAsteroid: AsteroidBase
    {
        public MetalAsteroid(Vector2 position, int metalAmount)
            : base(position, 0, metalAmount)
        {

        }

        public override string PrefabName
        {
            get
            {
                return ConstantsPrefabNames.MetalAsteroid;
            }
        }
    }
}
