﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Script.Model
{
    abstract class StructureBase : IStructure
    {

        public abstract string PrefabName { get; }
        public abstract void Tick();


        public StructureBase(Vector2 position)
        {
            this.Position = position;
            this.Dead = false;
        }
        public Vector2 Position
        {
            get; protected set;
        }

        public bool Dead
        {
            get; private set;
        }

        public bool Exploded
        {
            get; private set;
        }

        public event OnStructureModelChange OnChange;

        public void Die(bool exploded)
        {
            Dead = true;
            Exploded = exploded;
            CallOnChange();
        }

        protected void CallOnChange()
        {
            if (OnChange != null)
                OnChange(this);
        }


    }
}
