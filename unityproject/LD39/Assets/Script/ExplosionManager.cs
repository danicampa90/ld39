﻿using Assets.Script.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionManager : MonoBehaviour
{


    public void AddExplosionIn(Vector2 pos, float size)
    {
        GameObject prefab = null;
        try
        {
            prefab = Instantiate(Resources.Load<GameObject>(ConstantsPrefabNames.Explosion));
        }
        catch { prefab = Instantiate(Resources.Load<GameObject>(ConstantsPrefabNames.Unknown)); }
        prefab.transform.SetParent(this.transform);
        prefab.transform.position = (Vector3)pos+ new Vector3(0,0,-0.1f);
        prefab.transform.localScale = Vector3.one * size;
    }

    // Use this for initialization
    void Start()
    {
        Map.Instance.OnExplosion += AddExplosionIn;
    }
}
