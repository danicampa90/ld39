﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Script.Model;
using UnityEngine;


public class EnemyShipPrefabBehavior : MonoBehaviour, PrefabLogicScript
{

    float lastChange = 0;
    Vector2 oldPosition = new Vector2();
    Vector2 targetPosition = new Vector2();

    public void SetModelStructure(IStructure str)
    {
        str.OnChange += Str_OnChange;
    }

    private void Str_OnChange(IStructure structure)
    {
        if (structure.Dead)
        {
            structure.OnChange -= Str_OnChange;
            Destroy(this.gameObject);
        }
        oldPosition = this.transform.localPosition;
        targetPosition = structure.Position;


        Vector2 difference = targetPosition - oldPosition;
        if (difference != Vector2.zero)
        {
            float angle = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            this.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        lastChange = Time.time;
    }

    // Use this for initialization
    void Start()
    {
        oldPosition = this.transform.localPosition;
        targetPosition = this.transform.localPosition;
        this.lastChange = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        // this animates the ship towards the target position that's in the model.
        // the time taken to reach the target position is a tick.
        this.transform.localPosition = Vector2.Lerp(oldPosition, targetPosition, (Time.time - lastChange)/ Map.gameTickTime);
    }
}
