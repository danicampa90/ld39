﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Script.Model;
using UnityEngine;

public class ShooterPrefabLogic : MonoBehaviour, PrefabLogicScript
{
    public GameObject laserBeam;

    private Vector2 currentLaserTarget;

    public void SetModelStructure(IStructure str)
    {
        str.OnChange += Str_OnChange;
    }

    private void Str_OnChange(IStructure structure)
    {
        if (structure.Dead)
        {
            structure.OnChange -= Str_OnChange;
            Destroy(this.gameObject);
        }
        var shooter = (Shooter)structure;
        if (shooter.target != null) {
            currentLaserTarget = shooter.target.Position;


            var renderer = laserBeam.GetComponent<SpriteRenderer>();
            renderer.enabled = true;
            Vector3 direction = (Vector3)currentLaserTarget - transform.position;
            var distance = direction.magnitude;
            laserBeam.transform.localScale = new Vector2(distance / renderer.size.x, 1);
            //laserBeam.transform.localRotation = Quaternion.identity;

            if (direction != Vector3.zero)
            {
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                laserBeam.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }

            GetComponent<AudioSource>().Play();
            StartCoroutine(LaserShoot());
        }
    }

    private IEnumerator LaserShoot()
    {
        var renderer = laserBeam.GetComponent<SpriteRenderer>();
        float timeStarted = Time.time;
        while (Time.time < timeStarted + 1f)
        {
            
            yield return null;
            Color c = renderer.color;
            c.a = 1 - (Time.time - timeStarted) / 1f;
            renderer.color = c;
        }
        renderer.enabled = false;
    }
}
