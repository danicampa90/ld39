﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Script.Model;
using UnityEngine;

public class DefaultPrefabBehavior : MonoBehaviour, PrefabLogicScript {

    public void SetModelStructure(IStructure str)
    {
        str.OnChange += Str_OnChange;
    }

    private void Str_OnChange(IStructure structure)
    {
        if (structure.Dead)
        {
            structure.OnChange -= Str_OnChange;
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        this.transform.Rotate(Vector3.forward, UnityEngine.Random.Range(0, 1000));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
