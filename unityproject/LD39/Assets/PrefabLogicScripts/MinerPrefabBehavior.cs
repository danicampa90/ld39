﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Script.Model;
using UnityEngine;

public class MinerPrefabBehavior : MonoBehaviour, PrefabLogicScript
{
    float lastOperationTime;
    bool active = true;
    const float TimeToBecomeReadyInSec = (ConstantsGameBalance.MinerOperationEveryTicks * Map.gameTickTime);

    public GameObject laserBeam;
    public GameObject minerObject;

    private Vector2 currentLaserTarget;


    AudioSource audio
    {
        get
        {
            return this.GetComponent<AudioSource>();
        }
    }


    public void SetModelStructure(IStructure str)
    {
        str.OnChange += Str_OnChange;
    }

    private void Str_OnChange(IStructure structure)
    {
        if (structure.Dead)
        {
            structure.OnChange -= Str_OnChange;
            Destroy(this.gameObject);
        }
        var miner = ((Miner)structure);
        var active = miner.target != null;

        this.active = active;

        if (miner.target != null)
        {
            currentLaserTarget = miner.target.Position;
            
            var renderer = laserBeam.GetComponent<SpriteRenderer>();
            renderer.enabled = true;
            Vector3 direction = (Vector3)currentLaserTarget - transform.position;
            var distance = direction.magnitude;
            laserBeam.transform.localScale = new Vector2(distance / renderer.size.x, 1);
            //laserBeam.transform.localRotation = Quaternion.identity;

            if (direction != Vector3.zero)
            {
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                laserBeam.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }

            GetComponent<AudioSource>().Play();
            StartCoroutine(LaserShoot());
        }
    }

    private IEnumerator LaserShoot()
    {

        audio.Play();
        lastOperationTime = Time.time;

        var renderer = laserBeam.GetComponent<SpriteRenderer>();
        float timeStarted = Time.time;
        while (Time.time < timeStarted + 1f)
        {

            yield return null;
            Color c = renderer.color;
            c.a = 1 - (Time.time - timeStarted) / 1f;
            renderer.color = c;
        }
        renderer.enabled = false;

    }
    private void Awake()
    {
        lastOperationTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        // rotation (2PI)  * 2 times divided by the amount of time it takes to do a tick
        float progressToBecomingReady = (Time.time - lastOperationTime) / TimeToBecomeReadyInSec;
        if (progressToBecomingReady > 1)
            progressToBecomingReady = 1;
        float speed = (float)Math.PI * 4 * progressToBecomingReady;
        minerObject.transform.Rotate(Vector3.forward, speed);
        


    }
}
