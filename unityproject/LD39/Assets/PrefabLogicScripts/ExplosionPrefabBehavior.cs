﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionPrefabBehavior : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(RunUpdate());
	}

    private IEnumerator RunUpdate()
    {
        Vector3 originalScale = transform.localScale;
        float start = Time.time;
        float totalanimTime = 0.3f;
        var renderer = this.GetComponent<SpriteRenderer>();
        while(Time.time - start< totalanimTime)
        {
            float timeline = (Time.time - start) / totalanimTime;
            this.transform.localScale = Vector3.Lerp(Vector3.zero, originalScale, timeline);
            renderer.color = new Color(1, 1, 1, timeline);
            yield return null;

        }
        renderer.enabled = false;
        Destroy(this.gameObject);
    }
}
