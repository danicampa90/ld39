from flask import Flask,g,request
import json
import sqlite3
app = Flask(__name__)



def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect("DATABASE.sqlite3")
        create_schema(db)
        return db


def create_schema(db):
    statements = [  
        "CREATE TABLE IF NOT EXISTS scores ( name VARCHAR(50) PRIMARY KEY NOT NULL, score INT, killed INT, resources INT)",
        "",
        "CREATE INDEX IF NOT EXISTS scores_score ON scores(score)",
    ]
    for stat in statements:
        cur = db.execute(stat)
        cur.close()

    
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()



def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv



# schema:
@app.route('/scores', methods = ['GET', 'POST'])
def user():
    if request.method == 'GET':
        """return the information for <user_id>"""
        result = []
        for row in query_db("SELECT name, score, killed, resources FROM scores ORDER BY score DESC LIMIT 20"):
            result.append( {"name": row[0], "score": row[1], "kills":row[2], "metal": row[3] } )
        
        return json.dumps({"Items":result })
    
    if request.method == 'POST': 
        params = request.get_json(force=True, silent=True)
        if params is None:
            abort(405)

        print repr(params)

        name=params["name"]
        score=params["score"]
        kills=params["kills"]
        metal=params["metal"]
        

        db = get_db()
        cur = db.cursor()
        cur.execute('INSERT OR REPLACE INTO scores(name, score, killed, resources) VALUES (?,?,?,?)',(name, score, kills, metal))
        result = cur.execute('SELECT COUNT(*) FROM (SELECT name FROM scores WHERE score>?)',[score]).fetchone()[0]
        db.commit()
        
        return '{"positon": '+str(result)+'}'

    else:
        # POST Error 405 Method Not Allowed
        pass



if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=1337,
        debug=True,
        use_debugger=False,
        use_reloader=False
    )
