![logo.jpg](///raw/477/8/z/5bc7.jpg)


# Short description
This game is a simple strategy/tower defense game set in space. Playing time is around 5-15 minutes.
There are only 2 resources in this game:

- Metal
- Energy

Both can be obtained from the (many) asteroids that you can find around the base.
This game cannot be "win", at some point you will either be overwhelmed by the enemies or you will run out of resources to extract.
The objective of the game is to gather as much metal as possible, while not dying under the pressure of the enemies.

# Privacy disclaimer
Your score will be submitted to a database, in which you'll be able to compare with the rest of the players.
The server was coded during ludum dare and is currently hosted at my home, so it could go offline some times :smile:.


# Important
- Start by building METAL extractor, as if you run out of it, you cannot build extractors anymore 
  (yes, it would need a good rebalance, but I didn't know how to fix it without setting the cost to 0 (suggestions are welcome!). TL;DR: **Don't run out of metal or you'll not be able to build anything else**.
- When you run out of energy for too long you will still lose the game. There's no visual indication of this! If you don't hear turrets shooting then you have a problem with energy.
- You need to be paying attention closely to the energy level (or the amount of energy asteroids you are mining, when you are later in the game), as without energy the turrets cannot shoot...

### Tips
- Resources run out, you need to expand to stay alive, but expanding also increases your defenses requirements...
- Enemy kills count towards the score.
- if you use the same name your score will be REPLACED with the new one. You may go forward in the ladder, but you may also go back and worsen your score.
- the map is generated randomly (even though the number of asteroids is the same). You could find a map easier than another.
- The enemies are not intelligent at all, they just attack the nearest structure. They plan to win by overwhelming you with sheer numbers!
- Pay attention to enemy formations. At late stage their numbers become dangerous!

### Original ideas that I didn't implement due time constraints:
- Originally I wanted this game to have a power network like creeper world (look it up! It's an excellent game!) and a more interesting distribution of the asteroids (a bit more clustered - to force the player putting weak spots /links between the clusters), but there was really no time to get it up in time.
- I also wanted some more variety both in structures and enemies, but I already found it a bit challenging to reach an acceptable balance with the only enemy that is there, so adding more was out of the question.
